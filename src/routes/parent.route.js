const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const passport = require('../passport/passport');
const Parent = require('../model/parent.model');
const Child = require('../model/child.model');
const Location = require('../model/location.model');
const config = require('../config');
const verify = require('../middlewares/auth.middleware');
const moment = require('moment');

module.exports = (app) => {
    app.use('/parents', router);

    router.get('/me', verify, async (req, res) => {
        const user = await Parent.findById(req.tokenPayload.userId);
        return res.status(200).json(user);
    });

    router.patch('/info', verify, (req, res) => {
        const { display_name, email, date_of_birth, gender, avatar } = req.body;

        let UpdateParent = {
            $set: {
                email: email,
                date_of_birth: date_of_birth,
                gender: gender
            },
            $push: {
                display_name: {
                    type: 'update',
                    value: display_name,
                    time: moment().format()
                },
                avatar: {
                    type: 'update',
                    value: avatar,
                    time: moment().format()
                }
            }
        };

        Parent.findByIdAndUpdate(req.tokenPayload.userId, UpdateParent, (err, user) => {
            if (err) {
                return res.status(500).json(err);
            }
            if (!user) {
                return res.status(404).json({ message: 'Not found user: ' + req.tokenPayload.userId });
            }
            res.status(200).json({ message: 'Update successful' });
        });
    });

    router.get('/block', verify, async (req, res) => {
        const parent = await Parent.findById(req.tokenPayload.userId);

        if (!parent) {
            return res.status(401).json({ message: 'invalid token' });
        }

        parent.active = false;
        await parent.save();

        return res.status(200).json({ message: 'success' });
    });

    router.post('/child', verify, async (req, res) => {
        const child = new Child({ ...req.body });

        await Parent.findByIdAndUpdate(req.tokenPayload.userId, { $push: { children: child } });
        res.status(201).json({ message: 'success' });
    });

    router.get('/child/:child_key/location', verify, async (req, res) => {
        const parent = await Parent.findOne({ _id: req.tokenPayload.userId });
        const c = parent.children.find(x => x.key == req.params.child_key);
        res.status(200).json(c);
    });

    router.post('/child/:child_key/location', verify, async (req, res) => {
        let parent = await Parent.findOne({ _id: req.tokenPayload.userId });
        const idx = parent.children.findIndex(x => x.key == req.params.child_key);

        const location = new Location({ ...req.body });
        await location.save();
        parent.children[idx].locations.push(location);

        let UpdateParent = {
            $set: {
                children: parent.children
            }
        };

        await Parent.findByIdAndUpdate(req.tokenPayload.userId, UpdateParent);

        res.status(200).json({ message: 'success' });
    });
};