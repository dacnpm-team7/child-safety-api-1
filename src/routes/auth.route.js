const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const passport = require('../passport/passport');
const Parent = require('../model/parent.model');
const config = require('../config');
const moment = require('moment');

module.exports = (app) => {
    app.use('/auth', router);

    router.post('/signup', (req, res) => {
        const {username, password} = req.body;

        if (password.length < 6) {
            return res
                .status(400)
                .json({ message: 'Passwords must be at least 6 characters' });
        }

        Parent.findOne({ username: username }, (err, user) => {
            if (user) {
                return res.status(400).json({ message: 'Username has already been taken' })
            }

            const c = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            const randomKey = [...Array(6)].map(_ => c[~~(Math.random() * c.length)]).join('');

            bcrypt.hash(password, config.saltRounds, async function (err, hash) {
                if (err) { return res.status(500).json(err); }

                const user = new Parent({ username, key: randomKey });
                user.password.push({
                    type: 'create',
                    value: hash,
                    time: moment().format()
                })

                user.save((err, user) => {
                    if (err) { return res.status(500).json(err) }
                    const userModified = user.toObject();
                    delete userModified.password;
                    return res.status(201).json(userModified);
                });
            });
        });
    });

    router.post('/signin', (req, res) => {
        passport.authenticate('local', { session: false }, (err, user, info) => {
            if (err || !user) {
                return res.status(401).json({
                    message: info.message
                });
            }

            req.login(user, { session: false }, err => {
                if (err) {
                    return res.send(err);
                }
                const token = jwt.sign({ userId: user._id, username: user.username }, config.jwtSecret, { expiresIn: '7d' });
                return res.json({ access_token: token });
            });
        })(req, res);
    });
};