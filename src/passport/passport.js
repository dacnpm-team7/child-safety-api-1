const passport = require('passport');
const bcrypt = require('bcrypt');
const config = require('../config');
const User = require('../model/parent.model');

const LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy((username, password, done) => {
	User.findOne({ username: username }, (err, user) => {
		if (err) { return done(err); }
		if (!user) {
			return done(null, false, { message: 'Incorrect username or password' });
		}
		bcrypt.compare(password, user.password[user.password.length - 1].value, (err, isMatch) => {
			if (err) return done(err, null);
			if (isMatch) {
				return done(null, user);
			}
			const arr = [];
			arr.la
			return done(null, false, { message: 'Incorrect username or password' });
		});
	});
}));

module.exports = passport;