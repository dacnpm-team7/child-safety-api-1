const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Child = require('./child.model');

var ParentSchema = new mongoose.Schema(
    {
        username: String,
        password: [],
        children: [],
        key: String,
        display_name: [],
        email: String,
        date_of_birth: String,
        gender: String,
        facebook_id: String,
        google_id: String,
        avatar: [],
        role: { type: String, default: 'parent' },
        active: { type: Boolean, default: true }
    },
    {
        timestamps: true
    }
);

var parents = mongoose.model('parents', ParentSchema);

module.exports = parents;