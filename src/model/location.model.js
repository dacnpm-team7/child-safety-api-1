const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var LocationSchema = new mongoose.Schema(
    {
        longitude: String,
        latitude : String
    },
    {
        timestamps: true
    }
);

var locations = mongoose.model('locations', LocationSchema);

module.exports = locations;